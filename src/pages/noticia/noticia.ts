import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {NoticiaProvider} from '../../providers/noticia/noticia';

@Component({
  selector: 'page-noticia',
  templateUrl: 'noticia.html',
})
export class NoticiaPage {

  items: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, noticias: NoticiaProvider) {

    this.items = [];

      noticias.lista()
      .then((noticias) => {
         this.items = noticias;
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NoticiaPage');
  }

}
