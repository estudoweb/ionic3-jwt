import { Component } from '@angular/core';
import {MenuController, NavController, NavParams, ToastController} from 'ionic-angular';
import 'rxjs/add/operator/toPromise';
import {AuthProvider} from '../../providers/auth/auth';
import {HomePage} from '../home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = {
    email: null,
    password: null
  }

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public menuCtrl: MenuController,
              public toastCtrl: ToastController,
              private auth:AuthProvider) {

    this.menuCtrl.enable(false);
  }

  login() {
    console.log('LOGIN');
    this.auth.login(this.user)
      .then(() => {
          this.afterLogin();
      })
      .catch(() => {
        let toast = this.toastCtrl.create({
          message: 'Email e/ou senha inválidos.',
          duration: 3000,
          position: 'top',
          cssClass: 'toast-login-error'
        });
        toast.present(toast);
      })
  }

  afterLogin() {
    this.menuCtrl.enable(true);
    this.navCtrl.push(HomePage);
  }

}
