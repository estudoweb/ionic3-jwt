import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Auth} from '../../decorators/auth.decorator';
import {GeoProvider} from '../../providers/geo/geo';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
@Auth()

export class HomePage {

  public lat: number = 0;
  public lng: number = 0;

  constructor(public navCtrl: NavController) {



  }

}
