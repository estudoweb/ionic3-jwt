import {Injector} from '@angular/core';

let localInjector: Injector;

export const appContainer = (injector?:Injector) => {
    if(injector) {
        localInjector = injector;
    }

    return localInjector;
}
