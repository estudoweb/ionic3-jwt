import { Component, ViewChild } from '@angular/core';
import { Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { Redirector } from

import { HomePage } from '../pages/home/home';
import { NoticiaPage } from '../pages/noticia/noticia';
import { LoginPage } from '../pages/login/login';

import { AuthProvider} from '../providers/auth/auth';
import {GeoProvider} from '../providers/geo/geo';

// import md5 from 'cripto-md5';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any}>;
  user: any;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public auth: AuthProvider,
              public geo: GeoProvider
      ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Notícias', component: NoticiaPage },
      { title: 'Login', component: LoginPage },
    ];


  }


  initializeApp() {



    this.auth.user().then((user) => {
        this.user = user;

        console.log(user);
        this.geo.startTracking();

    });
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  // ngAfterViewInit() {
  //   // this.redirector.config(this.nav);
  // }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    this.auth.logout().then(() => {
      this.nav.push(LoginPage);
    })
  }
}
