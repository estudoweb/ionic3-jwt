import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { NoticiaPage } from '../pages/noticia/noticia';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { Http, HttpModule} from '@angular/http';
import { JwtClientProvider } from '../providers/jwt-client';
import { IonicStorageModule, Storage} from '@ionic/storage';
import { JwtHelper, AuthHttp, AuthConfig} from 'angular2-jwt';
import { AuthProvider } from '../providers/auth/auth';
import { Env } from '../models/env';
import { NoticiaProvider } from '../providers/noticia/noticia';
import { Geolocation } from '@ionic-native/geolocation';
import { GeoProvider } from '../providers/geo/geo';
import {BackgroundGeolocation} from '@ionic-native/background-geolocation';

declare var ENV: Env;

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    NoticiaPage,
    ListPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp,{}, {
      links: [
        {component: LoginPage, name: 'LoginPage' , segment: 'login'},
        {component: HomePage, name: 'HomePage' , segment: 'home'},
        {component: NoticiaPage, name: 'NoticiaPage' , segment: 'noticia'},
      ]
    }),
    IonicStorageModule.forRoot({
      driverOrder: ['localstorage']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    NoticiaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    JwtClientProvider,
    JwtHelper,
    AuthProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {
      provide: AuthHttp,
      deps: [Http, Storage],
      useFactory(http, storage) {
        let authConfig = new AuthConfig({
          headerPrefix: 'Bearer',
          noJwtError: true,
          noClientCheck: true,
          tokenGetter: (() => storage.get(ENV.TOKEN_NAME))
        });
        return new AuthHttp(authConfig, http);
      }
    },
    NoticiaProvider,
    GeoProvider,
    Geolocation,
    BackgroundGeolocation

  ]
})
export class AppModule {}
