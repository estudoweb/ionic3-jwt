import {Injectable, NgZone} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import {BackgroundGeolocation} from '@ionic-native/background-geolocation';
import {Geolocation, Geoposition} from '@ionic-native/geolocation';
import {AuthHttp} from 'angular2-jwt';

import {Env} from '../../models/env';
import {Response} from '@angular/http';

declare var ENV: Env;

/*
  Generated class for the GeoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GeoProvider {

  public watch: any;
  public lat: number = 0;
  public lng: number = 0;

  constructor(public zone: NgZone,
              public backgroundGeolocation: BackgroundGeolocation,
              private geolocation: Geolocation,
              public authHttp: AuthHttp) {

    this.geolocation.getCurrentPosition().then((resp) => {

      console.log(resp);
      // resp.coords.latitude
      // resp.coords.longitude
    }).catch((error) => {
      console.log('Error getting location', error);
    });


  }


  startTracking() {

    let config = {
      desiredAccuracy: 0,
      stationaryRadius: 20,
      distanceFilter: 10,
      debug: true,
      interval: 2000
    };

    this.backgroundGeolocation.configure(config).subscribe((location) => {

      console.log('BackgroundGeolocation:  ' + location.latitude + ',' + location.longitude);

      // Run update inside of Angular's zone
      this.zone.run(() => {
        this.lat = location.latitude;
        this.lng = location.longitude;
      });

    }, (err) => {

      console.log(err);

    });

    // Turn ON the background-geolocation system.
    this.backgroundGeolocation.start();


    // Foreground Tracking

    let options = {
      desiredAccuracy: 10,
      stationaryRadius: 20,
      distanceFilter: 30,
      interval: 60000
    };

    this.watch = this.geolocation.watchPosition(options).
          filter((p: any) => p.code === undefined)
          .subscribe((position: Geoposition) => {

          this.authHttp.post(`${ENV.API_URL}/geo`,{ geo: 'fabio teste' })
            .subscribe((response: Response) => {
              let sucesso = response.json();
              console.log(sucesso);
              return sucesso;
            });

      // Run update inside of Angular's zone
      this.zone.run(() => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
      });

    });
  }

  stopTracking() {
    console.log('stopTracking');

    this.backgroundGeolocation.finish();
    this.watch.unsubscribe();

  }

}
