import { Injectable } from '@angular/core';
import {  Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {AuthHttp } from 'angular2-jwt';
import {Env} from '../../models/env';

declare var ENV: Env;

/*
  Generated class for the NoticiaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NoticiaProvider {

  private _noticias = null;

  constructor(public authHttp: AuthHttp) {
    console.log('Hello NoticiaProvider Provider');
  }

  lista():Promise<Object>{
    return this.authHttp.get(`${ENV.API_URL}/noticia`,{})
      .toPromise()
      .then((response: Response) => {
        let noticias = response.json().data;
        this._noticias = noticias;
        return noticias;
      });
  }
}
