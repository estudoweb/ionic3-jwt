import {appContainer} from '../app/app.container';
import {AuthProvider} from '../providers/auth/auth';
import {Nav} from 'ionic-angular';
import {LoginPage} from '../pages/login/login';

export const Auth = () => {
    return (target: any) => {
        target.prototype.ionViewCanEnter = function() {

          let propertyNavController =
            Object
            .keys(this)
            .find(value => this[value] instanceof Nav);

            if(typeof propertyNavController === "undefined"){
              setTimeout(() => {
                //throw new TypeError("Auth decorator needs NavController instance");
              });

              return false;
            }

          let authService = appContainer().get(AuthProvider);

          authService.check().then(isLogged => {

            if(!isLogged) {
              setTimeout(() => {
                let navCtrl = this[propertyNavController];
                navCtrl.setRoot(LoginPage);
              });

              return false;
            }
            return true;
          })
        }
    }
}
